<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>ujian</title>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
  </head>
    <style>
  @-webkit-keyframes argh-my-eyes {
    0%   { background-color: #fff; }
    49% { background-color: #fff; }
    50% { background-color: #000; }
    99% { background-color: #000; }
    100% { background-color: #fff; }
  }
  @-moz-keyframes argh-my-eyes {
    0%   { background-color: #fff; }
    49% { background-color: #fff; }
    50% { background-color: #000; }
    99% { background-color: #000; }
    100% { background-color: #fff; }
  }
  @keyframes argh-my-eyes {
    0%   { background-color: #fff; }
    49% { background-color: #fff; }
    50% { background-color: #000; }
    99% { background-color: #000; }
    100% { background-color: #fff; }
  }
  body {
  -webkit-animation: argh-my-eyes 1s infinite;
  -moz-animation:    argh-my-eyes 1s infinite;
  animation:         argh-my-eyes 1s infinite;
}
</style>
  </body>
 </html>
